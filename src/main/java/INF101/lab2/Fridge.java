package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge  {
	
	
	private List<FridgeItem> itemsInFridge = new ArrayList<FridgeItem>();



	@Override
	public int nItemsInFridge() {
		return itemsInFridge.size();
		
	}

	@Override
	public int totalSize() {
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() >= totalSize()) {
			return false;	
		}
		return itemsInFridge.add(item);
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(!itemsInFridge.contains(item)) {
			throw new NoSuchElementException("Fridge do not contain item!");	
		}else {
			itemsInFridge.remove(item);
		}
		
	}

	@Override
	public void emptyFridge() {
		itemsInFridge.clear();
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
		for(FridgeItem item : itemsInFridge) {
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		
		itemsInFridge.removeAll(expiredFood);
		return expiredFood;
	}
	
	



}
